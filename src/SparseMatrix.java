import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;

/**
 * Created by MaksSklyarov on 02.05.17.
 */
public class SparseMatrix extends MyMatrix implements IMatrix {
    public LinkedList<MyList> list;
    private int rows;
    private int columns;
    private int size;

    public SparseMatrix(int i, int j){
        super(i,j);
        this.rows = i;
        this.columns = j;
        this.size = i*j;
        this.actual_cols = columns;
        list = new LinkedList<>();
        for(int d = 0;d<i*j;d++){
           // list.add(new MyList());
        }

    }
    @Override
    public void init(int value) {
        for (int i = 0;i!= size;i++) {
            for (int j = 0; j != size; j++) {
                this.setData(i,j,value );

            }
        }
    }

    @Override
    public void setData(int i, int j, int value) {
        int index = ((i-1)*columns) + j;
            ListIterator<MyList> it = list.listIterator();
            while(it.hasNext()){
                MyList tmp = it.next();
                if(tmp.x == index){
                    it.remove();
                   // System.out.println("Break");


                }

            }
          list.add(new MyList(index,value));
       // System.out.println("Добавили новый эл-т");

    }

    @Override
    public int getData(int i, int j) {
        int index = ((i-1)*columns) + j;
        ListIterator<MyList> it = list.listIterator();
        while(it.hasNext()){
            MyList tt = it.next();
            int x = tt.x;

            if(x==index){
                //System.out.println("data");
                return tt.data;}
        }
        //System.out.println("zero");
    return 0;
    }
//    @Override
//    public String toString() {
//        boolean app = false;
//        StringBuilder a = new StringBuilder();
//        for(int i = 0;i<rows*columns;i++){
//            app = false;
//            if(i%columns == 0)a.append("\n");
//            ListIterator<MyList> tt =list.listIterator();
//            while(tt.hasNext()){
//
//                MyList tmp = tt.next();
//                if(tmp.x==i) {
//
//                    a.append(tmp.data+ " ");
//                    app = true;
//                }
//
//
//            }
//            if(app == false){
//                 a.append(0 + " ");
//
//            }
//
//
//        }
//
//        return a.toString();
//    }
//    public SparseMatrix sum(SparseMatrix m2){
//        SparseMatrix tmp = new SparseMatrix(rows,columns);
//
//        for(int i = 0;i!=rows+1;i++){
//            for(int j = 0;j!=columns;j++){
//                //tmp.data[i][j] = this.data[i][j] + m2.data[i][j];
//                int tmp2 = this.getData(i,j) + m2.getData(i,j);
//                System.out.println(this.getData(i,j)+ "+" +m2.getData(i,j) + "="+ tmp2);
//                tmp.setData(i,j,this.getData(i,j)+m2.getData(i,j));
//
//            }
//        }
//        return tmp;
//    }
//    public SparseMatrix mul(SparseMatrix m2) {
//        if(this.columns != m2.rows){
//            throw new RuntimeException("Братан такие матрицы я не умножу.");
//
//        }
//        int m = rows;
//        int n = m2.columns;
//        int o = m2.rows;
//        SparseMatrix res = new SparseMatrix(m,n);
//
//        for (int i = 0; i < m+1; i++) {
//            for (int j = 0; j < n; j++) {
//                for (int k = 0; k < o; k++) {
//                    //res.data[i][j] += this.data[i][k] * m2.data[k][j];
//                    res.setData(i,j,res.getData(i,j)+(this.getData(i,k)*m2.getData(k,j)));
//                }
//            }
//        }
//        return res;
//    }
    public int getcols(){
        return columns;
    }
    public int getrows(){
        return rows;
    }
    public int getactualcols(){
        return actual_cols;
    }
}
