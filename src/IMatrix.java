/**
 * Created by MaksSklyarov on 02.05.17.
 */
public interface IMatrix {
    public void init(int value);
    public void setData(int i, int j, int value);
    public int getData(int i, int j);
    public int getcols();
    public int getrows();
    public int getactualcols();
    public IMatrix sum(IMatrix m2);
    public IMatrix mul(IMatrix m2);

}
