/**
 * Created by MaksSklyarov on 02.05.17.
 */
public class MySqMatrix extends MyMatrix implements IMatrix{
    public MySqMatrix(int row) {
        super(row, row);
        for(int i = 0; i < getrows();i++)
            this.setData(i,i,1);
    }

}

