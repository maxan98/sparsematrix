import java.util.TreeMap;

/**
 * Created by MaksSklyarov on 03.05.17.
 */
public class SparseTreeMatrix extends MyMatrix implements IMatrix {
    private int rows;
    private int columns;
    private int size;
    public TreeMap<Integer,Integer> list;
    public SparseTreeMatrix(int i, int j) {
        super(i, j);
        this.rows = i;
        this.columns = j;
        this.size = i*j;
        this.actual_cols = columns;
        list = new TreeMap<>();
    }

    @Override
    public void setData(int i, int j, int value) {
        int index = ((i-1)*columns) + j;
        list.put(index,value);


    }
    public int getData(int i, int j){
        int index = ((i-1)*columns) + j;
        if(list.containsKey(index)){
            return list.get(index);

        }
        return 0;
    }
}
